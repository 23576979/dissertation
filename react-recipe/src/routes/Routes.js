import React from 'react';
import Home from '../components/Home/Home';
import { Switch, Route, BrowserRouter as Router, BrowserRouter } from "react-router-dom";
import Recipe from '../components/Recipe/Recipe';
import Results from '../components/Results/Results';

const Routes = props => {
  return (
    <Router history={BrowserRouter}>
      <Switch>
        <Route path='/recipe/:recipename' component={Recipe}/>
        <Route path='/results/:searchterm' component={Results}/>
        <Route path='/' component={Home}/>
      </Switch>
    </Router>
  );
};

export default Routes;