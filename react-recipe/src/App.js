import React from 'react';
import logo from './logo.svg';
import Routes from './routes/Routes';

class App extends React.Component{
  render(){
    return (
      <div>
        <Routes baseUrl='/'/>
      </div>
    )
  }
}
export default App;