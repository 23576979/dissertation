import React from 'react';
import './Footer.scss';

const Footer = () => {
  return (
    <footer>
      <p className='footerText'>Created By Jake Leigh</p>
    </footer>
  )
};

export default Footer;