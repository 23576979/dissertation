import React from 'react';
import './Ingredients.scss';
import '../Search/Search.scss';
import {Link} from 'react-router-dom';

class Ingredients extends React.Component{
  state = {
    ingredients: [],
    combinedIngredient: []
  }
  componentWillMount(){
    let ingredients = JSON.parse(localStorage.getItem('ingredientsList'));
    if(ingredients == null || ingredients == undefined) ingredients = [];
    this.setState({ingredients});
    setTimeout(() => {
      this.calculateIngredients();
    })
  }
  calculateIngredients = () => {
    let ingredients = this.state.ingredients;
    let duplicatedIngredients = [];
    for(let i = 0; i < ingredients.length; i++){
      for(let j = 0; j < ingredients.length; j++){
        if(i !== j){
          if(ingredients[i][1] === ingredients[j][1]){
            duplicatedIngredients.push(ingredients[i]);
          }
        }
      }
    }
    if(duplicatedIngredients.length !== 0){
      this.combineIngredients(duplicatedIngredients);
    }
  }
  combineIngredients = (duplicatedIngredient) => {
    let combinedIngredient = [];
    let measurement = 0;
    let ingredient;
    for(let i = 0; i < duplicatedIngredient.length; i++){
      measurement = measurement + parseInt(duplicatedIngredient[i][0]);
      ingredient = duplicatedIngredient[1][1];
    }
    combinedIngredient.push([measurement, ingredient]);
    this.setState({combinedIngredient});
    this.addIngredient();
  }

  addIngredient = () => {
    let ingredients = this.state.ingredients;
    let combinedIngredient = this.state.combinedIngredient;
    let newIngredients = []
    for(let i = 0; i < ingredients.length; i++){
      if(ingredients[i][1] !== combinedIngredient[0][1]){
        newIngredients.push(ingredients[i]);
      }
    }
    newIngredients.push([combinedIngredient[0][0].toString(), combinedIngredient[0][1]]);
    this.setState({ingredients: newIngredients})
    localStorage.setItem('ingredientsList', JSON.stringify(newIngredients));
  }

  hideIngredientsPage = () => {
    this.props.hideIngredientsPage()
  }
  removeIngredient = e => {
    let ingredients = this.state.ingredients;
    for(let i = 0; i < ingredients.length; i++){
      if(i === parseInt(e.currentTarget.id)){
        ingredients.splice(i, 1);
      }
    }
    this.setState({ingredients});
    localStorage.setItem('ingredientsList', JSON.stringify(ingredients));
  }
  render(){
    const renderIngredients = this.state.ingredients.map((ingredient, key) => {
      return (
        <li onClick={this.removeIngredient} className='ingredientList' id={key}>
          <i className='ingredientMinus fas fa-minus'/>
      <span style={{fontSize: '16px'}}>{ingredient[0]}{ingredient[1]}</span>
        </li>
      )
    })
    return (
      <section className='searchPage'>
        {this.state.ingredients == null || this.state.ingredients.length == 0 ? (
          <section>
            <p onClick={this.hideIngredientsPage} className='cross'>X</p>
            <section className='noIngredientsPageSection'>
              <h1 className='noIngredientsTitle'>No ingredients added</h1>
              <h2 className='noIngredientsSub'>Go add some</h2>
              <Link to='/'>
                <button className='searchBtn'>
                  <i className='SearchArrow fas fa-arrow-right'/>
                </button>
              </Link>
            </section>
          </section>
        ) : (
          <section>
          <p onClick={this.hideIngredientsPage} className='cross'>X</p>
          <section className='ingredientsPageSection'>
            <h1 className='ingredientsTitle'>Ingredients</h1>
          </section>
          <section className='ingredientsPage'>
            {renderIngredients}
          </section>
          </section>
        )}
       
      </section>
    )
  }
};

export default Ingredients;
