import React from 'react';
import './RecipeCard.scss';
import StarRating from '../StarRating/StarRating';
import {Link} from 'react-router-dom';

class RecipeCard extends React.Component{
  render(){
    return (
      <Link to={`/recipe/${this.props.recipe}`} style={{textDecoration: 'none', color: 'inherit'}}>
      <section className='recipeCard'>
        <img className='recipeCardImage' src={this.props.image} alt={this.props.recipe}/>
        <section className='recipeCardData'>
          <h3 className='recipeCardTitle'>{this.props.recipe}</h3>
          <section className='recipeCardStarChef'>
              <StarRating  rating={this.props.starRating}/>
            <h4 className='recipeCardChef'>Chef - {this.props.chef.name}</h4>
          </section>
        </section>
      </section>
      </Link>
    )
  };
};

export default RecipeCard;