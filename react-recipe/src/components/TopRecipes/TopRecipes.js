import React from 'react';
import './TopRecipes.scss';
import recipeData from '../../json/recipes.json';
import RecipeCard from '../RecipeCard/RecipeCard';

class TopRecipes extends React.Component{
  state = {
    data: [],
    top4: []
  }
  componentWillMount(){
    this.setState({data: recipeData});
    setTimeout(() => {
      this.top4Config();
    }, 100)
  };

  top4Config = () => {
    let data = Object.values(this.state.data);
    let count = 0;
    let topRecipes = [];
    for(let i = 0; i < data.length; i++){
      count = count + 1;
      if(count < 5){
        topRecipes.push(data[i]);
      }
    }
    this.setState({top4: topRecipes});
  }
  render(){
    const renderTop4 = this.state.top4.map((recipe, key) => {
      return (
        <RecipeCard key={recipe.recipe_id} starRating={recipe.star_rating} recipe={recipe.recipe} image={recipe.recipe_image} chef={recipe.belongs_to_chef}/>
      )
    })
    return (
      <section>
        <h2 className='topRecipesTitle'>Todays Top Recipes</h2>
        <section className='topRecipesCards'>
        {renderTop4}
        </section>
      </section>
    )
  };
};

export default TopRecipes;