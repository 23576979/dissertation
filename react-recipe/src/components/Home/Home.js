import React from 'react';
import Navbar from '../Navbar/Navbar';
import splashImage from '../../assets/recipeImages/omelette.jpg'
import './Home.scss';
import Footer from '../Footer/Footer';
import TopRecipes from '../TopRecipes/TopRecipes';
import Collections from '../Collections/Collections';
import ChefCards from '../ChefCards/ChefCards';
import {Link} from 'react-router-dom'
import Search from '../Search/Search';
import Ingredients from '../Ingredients/Ingredients';

class Home extends React.Component{
  state = {
    showSearchPage: false,
    showIngredientsPage: false
  }
  showSearchPage = () => {
    this.setState({showSearchPage: true})
  };
  hidePage = () => {
    this.setState({showSearchPage: false})
  }
  showIngredientsPage = () => {
    this.setState({showIngredientsPage: true})
   }
   hideIngredientsPage = () => {
     this.setState({showIngredientsPage: false})
   }
  render(){
    const renderPage = () => {
      if(this.state.showSearchPage === true){
        return (
          <Search hidePage={this.hidePage}/>
        )
      } else if(this.state.showIngredientsPage === true){
        return (
          <Ingredients hideIngredientsPage={this.hideIngredientsPage}/>
        )
      } else {
        return (
        <section>
          <header>
          <Navbar showSearchPage={this.showSearchPage} showIngredientsPage={this.showIngredientsPage}/>
        </header>
        <Link to={`/recipe/${'Ham and cheese omelette'}`} style={{textDecoration: 'none', color: 'inherit'}}>
          <section className='splashRecipe'>
            <img className='splashImage' src={splashImage} alt='ham and cheese omelette'/>
            <h1 className='splashTitle'>Ham and Cheese Omelette</h1>
          </section>
        </Link>
        <TopRecipes/>
        <Collections/>
        <ChefCards />
        <Footer />
        </section>
        )
      }
    }
    return (
      <section>
        {renderPage()}
      </section>
    )
  };
};

export default Home;