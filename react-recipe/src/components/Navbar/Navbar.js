import React from 'react';
import './Navbar.scss'
import Logo from '../../assets/Logos/unifood-white-text.png';
import Search from '../Search/Search';
import {Link} from 'react-router-dom';
import Ingredients from '../Ingredients/Ingredients';
 
class Navbar extends React.Component{ 
  state = {
    showSearchPage: false,
    showIngredientsPage: false
  }
  showSearchPage = () => {
    this.props.showSearchPage();
  };
  hidePage = () => {
    this.setState({showSearchPage: false})
  }
  showIngredientsPage = () => {
   this.props.showIngredientsPage()
  }
  hideIngredientsPage = () => {
    this.setState({showIngredientsPage: false})
  }
  render(){
    return (
      <section>
        {this.state.showIngredientsPage === true ? (
          <Ingredients hideIngredientsPage={this.hideIngredientsPage}/>
        ):(<p></p>)}
        <nav> 
          <Link to='/'><img className='logo' src={Logo} /></Link>
          <i onClick={this.showIngredientsPage} className="navIcon fas fa-shopping-cart"></i>
          <i onClick={this.showSearchPage} className="navIcon fas fa-search"></i>
        </nav>
        </section>
    )
  }
};
export default Navbar;