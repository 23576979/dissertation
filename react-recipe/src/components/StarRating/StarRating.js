import React from 'react';
import './StarRating.scss';
import star1 from '../../assets/1star.png';
import star2 from '../../assets/2star.png';
import star3 from '../../assets/3star.png';
import star4 from '../../assets/4star.png';
import star5 from '../../assets/5star.png';

class StarRating extends React.Component{
  state = {
    starRating: ''
  }
  componentDidMount(){
    let rating = this.props.rating;
    if(rating === undefined){
      setTimeout(() => {
        this.calculatRating();
      }, 100)
    }
    if(rating === 1){
      this.setState({starRating: star1});
    } else if(rating === 2){
      this.setState({starRating: star2});
    } else if(rating === 3){
      this.setState({starRating: star3});
    }
    else if(rating === 4){
      this.setState({starRating: star4});
    }
    else if(rating === 5){
      this.setState({starRating: star5});
    }
  }

  calculatRating = () => {
    let rating = this.props.rating
    if(rating === 1){
      this.setState({starRating: star1});
    } else if(rating === 2){
      this.setState({starRating: star2});
    } else if(rating === 3){
      this.setState({starRating: star3});
    }
    else if(rating === 4){
      this.setState({starRating: star4});
    }
    else if(rating === 5){
      this.setState({starRating: star5});
    }
  }
  
  render(){
    return (
      <section>
        <img className='starRating' src={this.state.starRating} />
      </section>
    )
  };
};

export default StarRating;