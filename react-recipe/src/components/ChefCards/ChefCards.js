import React from 'react';
import json from '../../json/recipes.json';
import ChefCard from '../ChefCard/ChefCard';

class ChefCards extends React.Component{
  state = {
    data: [],
    chefs: []
  }
  componentWillMount(){
    this.setState({data: json});
    setTimeout(() =>{
      this.chefsConfig();
    }, 100)
  };
  chefsConfig = () => {
    let data = Object.values(this.state.data);
    let chefs = [];
    for(let i = 0; i < data.length; i++){
        chefs.push(data[i].belongs_to_chef);
    }
    this.setState({chefs})
  }
  render(){
    let num = 0;
    const renderChefs = this.state.chefs.map(chef => {
      num = num + 1;
      if(num < 5){
        return (
          <ChefCard key={chef.name} name={chef.name} image={chef.image} recipes={chef.recipes} />
        )
      }
    })
    return (
      <section>
        <h2 className='topRecipesTitle'>Meet The Chefs</h2>
        <section className='topRecipesCards'>
          {renderChefs}
        </section>
      </section>
    )
  }
};

export default ChefCards;