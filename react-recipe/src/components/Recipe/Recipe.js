import React from 'react';
import json from '../../json/recipes.json';
import Navbar from '../Navbar/Navbar';
import Footer from '../Footer/Footer';
import './Recipe.scss';
import StarRating from '../recipeStarRating/StarRating';
import Search from '../Search/Search';
import Ingredients from '../Ingredients/Ingredients';

class Recipe extends React.Component{
  state = {
    json: [],
    recipeTitle: [],
    recipeData: [],
    ingredients: [], 
    instructions: [],
    showSearchPage: false,
    showIngredientsPage: false
  }
  componentWillMount(){
    this.setState({json});
    this.setState({recipeTitle: this.props.match.params.recipename});
    setTimeout(() => {
      this.renderRecipe();
      this.renderIngredients();
      this.renderInstructions();
    }, 100)
  }
  renderRecipe = () => {
    let data = Object.values(this.state.json);
    let recipeTitle = this.state.recipeTitle;
    for(let i = 0; i < data.length; i++){
      if(recipeTitle === data[i].recipe){
        this.setState({recipeData: data[i]})
      }
    }
  }
  updateServingsButton = (e) => {
    if(e.target.id === 'minus'){
      let recipeData = {... this.state.recipeData};
      recipeData.serves = recipeData.serves - 1;
      this.setState({recipeData});
    } else {
      let recipeData = {... this.state.recipeData};
      recipeData.serves = recipeData.serves + 1;
      this.setState({recipeData});
    }
  }
  updateServings = (e) => {
    let recipeData = {... this.state.recipeData};
    recipeData.serves = e.target.value;
    this.setState({recipeData})
  }
  renderIngredients = () => {
    let ingredients = Object(this.state.recipeData.ingredients);
    let array = [];
    for(let i = 0; i < ingredients.length; i++){
      array.push(ingredients[i])
    }
   this.setState({ingredients: array});
  }
  renderInstructions = () => {
    let instructions = Object(this.state.recipeData.instructions);
    let array = [];
    for(let i = 0; i < instructions.length; i++){
      array.push(instructions[i]);
    }
    this.setState({instructions: array});
  }
  addIngredient = e => {
    let ingredients = JSON.parse(localStorage.getItem('ingredientsList'));
    if(ingredients == null) ingredients = [];
    let measurement = e.currentTarget.dataset.value;
    let ingredient = e.currentTarget.dataset.id;
    ingredients.push([measurement, ingredient]);
    localStorage.setItem('ingredientsList', JSON.stringify(ingredients));
  }
  addAll = e => {
    e.preventDefault();
    let allIngredients = this.state.ingredients;
    let ingredients = JSON.parse(localStorage.getItem('ingredientsList'));
    if(ingredients == null) ingredients = [];
    for(let i = 0; i < allIngredients.length; i++){
      ingredients.push([allIngredients[i].measurement, allIngredients[i].ingredient]);
    }
    localStorage.setItem('ingredientsList', JSON.stringify(ingredients));
    console.log(JSON.parse(localStorage.getItem('ingredientsList')))
  }
  showSearchPage = () => {
    this.setState({showSearchPage: true})
  };
  hidePage = () => {
    this.setState({showSearchPage: false})
  }
  showIngredientsPage = () => {
    this.setState({showIngredientsPage: true})
   }
   hideIngredientsPage = () => {
     this.setState({showIngredientsPage: false})
   }
  render(){
    const renderIngredients = this.state.ingredients.map(ingredient => {
      return (
        <li onClick={this.addIngredient} data-value={ingredient.measurement} data-id={ingredient.ingredient} className='ingredientList'>
          <i className='ingredientPlus fas fa-plus'/>
          <span>{ingredient.measurement}</span><span>{ingredient.ingredient}</span>
        </li>
      )
    })
    const renderInstructions = this.state.instructions.map(instruction => {
      return (
        <li className='instructionList'>
          {instruction}
        </li>
      )
    })
    const renderPage = () => {
      if(this.state.showSearchPage === true){
        return (
          <Search hidePage={this.hidePage}/>
        )
      } else if(this.state.showIngredientsPage === true){
        return (
          <Ingredients hideIngredientsPage={this.hideIngredientsPage}/>
        )
      } else {
        return (
        <section>
          <header>
        <Navbar showSearchPage={this.showSearchPage} showIngredientsPage={this.showIngredientsPage}/>
        </header>
        <section className='recipeSplash'>
          <h1 className='recipeTitle'>{this.state.recipeData.recipe}</h1>
          <section className='starDietary'>
            <StarRating rating={this.state.recipeData.star_rating}/>
            <h2 className='dietary'>{this.state.recipeData.dietary}</h2>
          </section>
          <img src={this.state.recipeData.recipe_image} className='recipeImage' alt={this.state.recipeData.recipe}/>
        </section>
        <section className='recipeData'>
          <section className='prepCook'>
            <section>
              <p className='timeText'>Prep Time</p>
              <h3 className='timeData'>{this.state.recipeData.prep_time}</h3>
            </section>
            <section>
              <p className='timeText'>Cook Time</p>
              <h3 className='timeData'>{this.state.recipeData.cook_time}</h3>
            </section>
          </section>
          <h4 className='recipeDescription'>{this.state.recipeData.description}</h4>
        </section>
        <section className='ServesSection'>
    <p className='servesText'>Serves: {this.state.recipeData.serves}</p>
        </section>
        <section className='ingredientsSection'>
          <h2 className='topRecipesTitle'>Ingredients</h2>
          <section className='ingredients'>
            {renderIngredients}
            <button onClick={this.addAll} className='addAllButton'>Add all</button>
          </section>
        </section>
        <section className='instructionsSection'>
          <h2 className='topRecipesTitle'>Instructions</h2>
          <ol>
            {renderInstructions}
          </ol>
        </section>
        <section className='nuritionSection'>
          <h2 className='topRecipesTitle'>Nutrition Facts</h2>
    <p className='nutrients'>{this.state.recipeData.food_nutrients}</p>
        </section>
        <Footer />
        </section>
        )
      }
    }
    return (
      <section>
        {renderPage()}
      </section>
    )
  };
};
export default Recipe;