import React from 'react';
import './Collection.scss';

class Collection extends React.Component{
  render(){
    return (
      <section>
        <section className='collection'>
          <section className='collectionData'>
            <h3>{this.props.title}</h3>
            <p>Num of Recipes: {this.props.num}</p>
          </section>
          <section className='collectionImage'>
            <img src={this.props.image} alt={this.props.title}/>
          </section>
        </section>
      </section>
    )
  }
};

export default Collection;