import React from 'react';
import './Collections.scss';
import Collection from '../Collection/Collection';
import json from '../../json/recipes.json';

class Collections extends React.Component{
  state = {
    data: [],
    veggie: [],
    chicken: [],
    quick: []
  }
  componentWillMount(){
    this.setState({data: json});
    setTimeout(() => {
      this.veggie();
      this.chicken();
      this.quick();
    },100);
  }
  veggie = () => {
    let data = Object.values(this.state.data);
    let veggie = [];
    for (let i = 0; i < data.length; i++){
      if(data[i].dietary === 'V'){
        veggie.push(data[i])
      }
    }
    this.setState({veggie})
  }
  chicken = () => {
    let data = Object.values(this.state.data);
    let chicken = [];
    for (let i = 0; i < data.length; i++){
      if(data[i].recipe.includes('Chicken')){
        chicken.push(data[i])
      }
    }
    this.setState({chicken})
  }
  quick = () => {
    let data = Object.values(this.state.data);
    let quick = [];
    for (let i = 0; i < data.length; i++){
      if(data[i].cook_time.includes('10')){
        quick.push(data[i])
      }
    }
    this.setState({quick})
  }
  showResults = e => {
    if(e.currentTarget.id === 'veggie'){
      localStorage.setItem('results', JSON.stringify(this.state.veggie));
      localStorage.setItem('searchTerm', 'Veggie');
      window.location.href = '/results/' + 'Veggie';
    } else if(e.currentTarget.id === 'chicken'){
      localStorage.setItem('results', JSON.stringify(this.state.chicken));
      localStorage.setItem('searchTerm', 'Chicken');
      window.location.href = '/results/' + 'Chicken';
    } else {
      localStorage.setItem('results', JSON.stringify(this.state.quick));
      localStorage.setItem('searchTerm', 'Quick');
      window.location.href = '/results/' + 'Quick';
    }
  }
  render(){
    return (
      <section className='collections'>
        <h2 className='topRecipesTitle'>Collections</h2>
        <section id='veggie' onClick={this.showResults}>
        <Collection  image='https://i.ibb.co/xJbvxqx/egg-on-toast.jpg' title='Veggie Dishes' num={this.state.veggie.length} />
        </section>
        <section id='chicken' onClick={this.showResults}>
        <Collection  image='https://i.ibb.co/mh67wSq/chow-mein.jpg' title='Chicken Dishes' num={this.state.chicken.length} />
        </section>
        <section id='quick' onClick={this.showResults}>
        <Collection  image='https://i.ibb.co/4mLms8n/pizza-toast.jpg' title='Quick Recipes' num={this.state.quick.length} />
        </section>
      </section>
    )
  };
};

export default Collections;