import React from 'react';
import './Results.scss';
import Navbar from '../Navbar/Navbar';
import Footer from '../Footer/Footer';
import RecipeCard from '../RecipeCard/RecipeCard';
import Search from '../Search/Search';
import Ingredients from '../Ingredients/Ingredients';

class Results extends React.Component{
  state = {
    results: [],
    searchTerm: '',
    numResults: 0,
    showSearchPage: false,
    showIngredientsPage: false
  }
  componentWillMount(){
    let results = JSON.parse(localStorage.getItem('results'));
    this.setState({results})
    let searchTerm = localStorage.getItem('searchTerm');
    this.setState({searchTerm})
    setTimeout(() => {
      this.resultsConfig();
    }, 100)
  }
  resultsConfig = () => {
    let results = Object.values(this.state.results);
    let numResults = 0;
    for(let i = 0; i < results.length; i++){
      numResults = numResults + 1;
    }
    this.setState({numResults})
  }
  showSearchPage = () => {
    this.setState({showSearchPage: true})
  };
  hidePage = () => {
    this.setState({showSearchPage: false})
  }
  showIngredientsPage = () => {
    this.setState({showIngredientsPage: true})
   }
   hideIngredientsPage = () => {
     this.setState({showIngredientsPage: false})
   }
  render(){
    const results = this.state.results.map((recipe, key) => {
      return (
        <RecipeCard key={recipe.recipe_id} starRating={recipe.star_rating} recipe={recipe.recipe} image={recipe.recipe_image} chef={recipe.belongs_to_chef}/>
      )
    })
    const renderPage = () => {
      if(this.state.showSearchPage === true){
        return (
          <Search hidePage={this.hidePage}/>
        )
      } else if(this.state.showIngredientsPage === true){
        return (
          <Ingredients hideIngredientsPage={this.hideIngredientsPage}/>
        )
      } else {
        return (
        <section>
        <header>
          <Navbar showSearchPage={this.showSearchPage} showIngredientsPage={this.showIngredientsPage}/>
          </header>
          <section className='recipeSplash'>
          <h1 className='recipeTitle'>{this.state.searchTerm}</h1>
          <h2 className='numResults'>Num of recipes: {this.state.numResults}</h2>
          </section>
          <section className='topRecipesCards'>
            {results}
          </section>
          <Footer />
        </section>
        )
      }
    }
    return (
      <section>
      {renderPage()}
    </section>
    )
  }
};
export default Results;