import React from 'react';
import './Search.scss';
import json from '../../json/recipes.json';

class Search extends React.Component{
  state = {
    searchTerm: '',
    json: [],
    searchedRecipes: []
  };
  componentWillMount(){
    this.setState({json})
  }
  hidePage = () => {
    this.props.hidePage()
  }
  saveSearchTerm = e => {
    this.setState({searchTerm: e.target.value});
  }
  searchRecipes = e => {
    e.preventDefault();
    let search = this.state.searchTerm;
    let json = Object.values(this.state.json);
    let searchedRecipes = [];
    for(let i = 0; i <json.length; i++){
      if(json[i].recipe.includes(search)){
        searchedRecipes.push(json[i])
      }
    } 
    this.setState({searchedRecipes})
    setTimeout(() => {
      localStorage.setItem('results', JSON.stringify(this.state.searchedRecipes));
      localStorage.setItem('searchTerm', this.state.searchTerm);
      window.location.href = '/results/' + this.state.searchTerm;
    }, 100)
  }
  render(){
    return (
      <section className='searchPage'>
       <p onClick={this.hidePage}className='cross'>X</p>
       <form className='searchForm'>
         <section className='form'>
           <input onChange={this.saveSearchTerm} type='text' placeholder='Search for a recipe' />
           <button onClick={this.searchRecipes} className='searchBtn'>
             <i className='SearchArrow fas fa-arrow-right'/>
           </button>
         </section>
       </form>
       <section className='suggestions'>
         <h1 className='suggestion'>Suggestions</h1>
         <ul>
           <li>- Vegan bean and quinoa salad</li>
           <li>- Chicken and mushroom creamy pasta</li>
           <li>- Beef korma with rice</li>
           <li>- Chicken and green bean stir fry</li>
         </ul>
       </section>
      </section>
    )
  }
};
export default Search;