import React from 'react';
import './ChefCard.scss';
import json from '../../json/recipes.json';

class ChefCard extends React.Component{
  state = {
    chefName: '',
    json: [],
    recipes: []
  }
  componentWillMount(){
    this.setState({chefName: this.props.name});
    this.setState({json});
    setTimeout(() => {
      this.getRecipes();
    }, 100);
  }
  getRecipes = () => {
    let json = Object.values(this.state.json);
    let chefName = this.state.chefName;
    let recipes = [];
    for(let i = 0; i < json.length; i++){
      if(json[i].belongs_to_chef.name === chefName){
        recipes.push(json[i])
      }
    }
    this.setState({recipes})
  }
  showResults = () => {
    localStorage.setItem('results', JSON.stringify(this.state.recipes));
    localStorage.setItem('searchTerm', this.props.name);
    window.location.href = '/results/' + this.props.name
  }
  render(){
    return (
      <section className='recipeCard' onClick={this.showResults}>
        <img className='recipeCardImage' src={this.props.image} alt={this.props.name}/>
        <section className='recipeCardData'>
          <h3 className='recipeCardTitle'>{this.props.name}</h3>
          <section>
            <h4 className='recipeNum'>Recipes - {this.props.recipes}</h4>
          </section>
        </section>
      </section>
    )
  }
}
export default ChefCard;