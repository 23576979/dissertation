import Home from './components/Home.vue'
import Recipe from './components/Recipe.vue'
import Results from './components/Results.vue'
export const routes = [
  {
    path: '/recipe/:recipename', component: Recipe, props:  true
  },
  {
    path: '/results/:searchterm', component: Results, props: true
  },
    {
    path: '', component: Home
  }
]