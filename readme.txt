Hello, these are instructions as to how to run both applications.

Using the terminal on mac or command prompt on windows, open each of the project folders called 'react-recipe' and 'vue-recipe' and run the command 'npm install' this will install all of the dependencies needed to run the application. 

Once this has been installed, simply type the command 'serve -s dist' for each of the projects which will run the build server using localhost. 